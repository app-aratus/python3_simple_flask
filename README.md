# README #


A Simple Flask Application

### What is this repository for? ###

* Quick summary

This is an ultra simple example of a Flask application.  You could build on this to create either a microservice or a website.

* Version

1.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* How do I set up the environment on a MacOSX?

Install the Python 3 environment with Anaconda

pip install flask

* How do I run it?

./start_flask.sh

./test_flask.sh

or

point your browser to- 

http://127.0.0.1:5000/

* What file is being called when it is run?

runflask.py

* What does the output look like when you run it?

./test_flask.py 

curl http://127.0.0.1:5000/

Hello, World!

### Snapshots ###

* Startup From MacOSX Terminal
![Flask application template startup from command line](images/simple_flask_app_start_from_command_line.png)

* Test From MacOSX Terminal
![Flask application template test from command line](images/simple_flask_app_test_from_command_line.png)

* Index Page From A Browser
![Flask application template index page from browser](images/simple_flask_app_from_browser.png)





